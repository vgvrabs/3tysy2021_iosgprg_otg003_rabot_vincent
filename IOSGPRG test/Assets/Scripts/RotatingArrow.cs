﻿using UnityEngine;

public class RotatingArrow : Arrow {
    public Enemy parent;

    void OnTriggerEnter2D(Collider2D col){
        PickDirection();
    }

    void Start() {
        parent = GetComponentInParent<Enemy>();
        //PickDirection();
    }

    void RandomizeDirection(){
        if(!parent.isInRange){
            transform.localRotation *= Quaternion.Euler(0,0, 90);
        }
        if (parent.isInRange){
            PickDirection();
        }
    }


    void Update() { 
        RandomizeDirection();
    }

    public override void PickDirection() {
        this.transform.localEulerAngles.Normalize();
        if (this.transform.localEulerAngles.z == 270){
            enemyDirection = swipeDirection.Up;
        }
        else if (this.transform.localEulerAngles.z == 90){
            enemyDirection = swipeDirection.Down;
        }
        else if (this.transform.localEulerAngles.z == 180){
            enemyDirection = swipeDirection.Right;
        }
        else if (this.transform.localEulerAngles.z == 0) {
            enemyDirection = swipeDirection.Left;
        }
    }
    
}
