﻿using UnityEngine;
public class PlayerHealth : MonoBehaviour {

    [SerializeField] bool isAlive = true;
    public HealthText healthText;
    public int health;


    public void checkIfAlive(){
        if (health <= 0) {
            isAlive = false;
            FindObjectOfType<GameManager>().GameOver();
            gameObject.SetActive(false);
        }
    }

    public int takeDamage(int damage){
       health -= damage;
       health = Mathf.Clamp(health, 0, 3);
       healthText.displayHealth();
       checkIfAlive();
       return health;
    }

    public int addHp(int value){
        health += value;
        health = Mathf.Clamp(health, 0, 3);
        healthText.displayHealth(); 
        return health;   

    }
}
