﻿using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour {

    public Spawner spawner;
    int manaCost = 100;

    public float slowDownFactor;
    public float slowDownLength;
    public PlayerMana player;



    void Update(){
    
    }

    public void UseSkill(){
        // input condition for player resource
        if(player.currentMana == manaCost){
            StartCoroutine(WaitThenRestoreTime());
            Debug.Log("Used Skill!");
            foreach(GameObject enemy in spawner.enemies){
                Destroy(enemy);
            }
            player.useSkill(100);
            //player.manaText
        }
        else {
            Debug.Log("Not enough mana!");
        }
    }

    private IEnumerator WaitThenRestoreTime(){
        Time.timeScale = slowDownFactor;
        yield return new WaitForSecondsRealtime(slowDownLength);

        Time.timeScale = 1f;

    }
}
