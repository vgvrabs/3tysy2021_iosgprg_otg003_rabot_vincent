﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    [SerializeField] bool gameHasEnded = false;
    public GameObject menu;
    public PlayerHealth player;

    void Start(){

    }

    public void GameOver(){
        if(gameHasEnded == false){
            gameHasEnded = true;
            menu.SetActive(true);
            Debug.Log("Game Over");
        }
    }
}
