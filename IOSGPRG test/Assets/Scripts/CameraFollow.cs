﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform player;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    void LateUpdate() {
        this.transform.position = new Vector3(player.position.x + 3,transform.position.y,transform.position.z);
        
    }
}
