﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score : MonoBehaviour {

    public Transform player;
    public TextMeshProUGUI text;

    void Update() {
        text.text = "Score:" + player.position.x.ToString("0");
    }
}
