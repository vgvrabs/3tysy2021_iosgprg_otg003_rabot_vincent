﻿using UnityEngine;

public class HealthPack : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Player"){
            col.GetComponent<PlayerHealth>().addHp(1);
            Destroy(gameObject);
        }

    }
}
