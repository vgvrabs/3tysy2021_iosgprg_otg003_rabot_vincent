﻿using UnityEngine;

public class ScrollingBackground : MonoBehaviour { 
    public float speed = 0.1f;
    public Renderer bgRenderer;


    void Start(){
    }

    void Update(){
        bgRenderer.material.mainTextureOffset += new Vector2(speed * Time.deltaTime, 0f);
    }

}
