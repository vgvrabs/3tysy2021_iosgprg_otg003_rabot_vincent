﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {

    public Transform spawnPoint;
    public int size;
    public GameObject[] enemyPrefab;
    public GameObject healthPackPrefab;
    public List<GameObject> enemies = new List<GameObject>();
    int randomSpawnPoint, random;
    public bool spawnAllowed;
    public float spawnTime;
    public float firstSpawn;
    public float spawnTimeMax;

    void Start(){
        Invoke("SpawnEnemy", 1f);
        Invoke("SpawnHealthPack", 15f);
    }

   void SpawnEnemy(){
       GameObject enemy = Instantiate(enemyPrefab[Random.Range(0, size)]);
       enemy.transform.position = new Vector2(spawnPoint.position.x, spawnPoint.position.y);
       AddEnemy(enemy);
       Invoke("SpawnEnemy", Random.Range(0.5f, 2f));
    }


    void SpawnHealthPack(){
        GameObject healthPack = Instantiate(healthPackPrefab);
        healthPack.transform.position = new Vector2(spawnPoint.position.x, spawnPoint.position.y);
        Invoke("SpawnHealthPack", Random.Range(10f, 30f));
    }

    void AddEnemy(GameObject enemy){
        enemies.Add(enemy);
    }
}
