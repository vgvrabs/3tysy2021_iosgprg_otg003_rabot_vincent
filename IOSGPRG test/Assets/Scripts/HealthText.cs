﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthText : MonoBehaviour {

    public PlayerHealth player;
    public TextMeshProUGUI sampleText;

    void Start() {
        displayHealth();
    }
    

    public void displayHealth(){
        sampleText.text = "Health:" + player.health;
    }
}
