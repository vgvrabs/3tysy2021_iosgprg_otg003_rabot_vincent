﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ManaText : MonoBehaviour {

    public PlayerMana player;
    public TextMeshProUGUI text;

    void Start() {
        displayMana();
    }


    public void displayMana(){
        text.text = "Mana:" + player.currentMana + "/" + player.maxMana;
    }

}
