﻿using UnityEngine;

public class PlayerMana : MonoBehaviour {
    public ManaText manaText;
    public int currentMana = 0;
    public int maxMana = 100;

    public int addMana(int value){
        currentMana += value;
        currentMana = Mathf.Clamp(currentMana, 0, maxMana);
        manaText.displayMana();
        return currentMana;
    }

    public int useSkill(int value) {
        currentMana -= value;
        currentMana = Mathf.Clamp(currentMana, 0, maxMana);
        manaText.displayMana();
        return currentMana;
    }
}
