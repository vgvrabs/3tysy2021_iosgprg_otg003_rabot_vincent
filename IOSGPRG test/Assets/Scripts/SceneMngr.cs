﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMngr : MonoBehaviour {

    public void RestartGame(){
        SceneManager.LoadScene("Game");
        Time.timeScale = 1f;
    }

    public void CharSelect(){
        SceneManager.LoadScene("Character Select");
    }
    
}
