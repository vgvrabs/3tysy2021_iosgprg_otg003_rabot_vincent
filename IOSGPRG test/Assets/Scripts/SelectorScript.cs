﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SelectorScript : MonoBehaviour {

    public GameObject Knight;
    public GameObject HeavyBandit;
    public GameObject LightBandit;

    private Vector3 characterPos;
    private Vector3 offScreen;
    [SerializeField] int characterInt = 1;
    SpriteRenderer knightRenderer;
    SpriteRenderer heavyBanditRenderer;
    SpriteRenderer lightBanditRenderer;
    [SerializeField] readonly string selectedCharacter = "SelectedCharacter";

    private void Awake(){
        characterPos = Knight.transform.position;
        offScreen = HeavyBandit.transform.position;
        knightRenderer = Knight.GetComponent<SpriteRenderer>();
        heavyBanditRenderer = HeavyBandit.GetComponent<SpriteRenderer>();
        lightBanditRenderer = LightBandit.GetComponent<SpriteRenderer>();
    }

    public void NextCharacter(){

        switch(characterInt){
            case 0:
                PlayerPrefs.SetInt(selectedCharacter, 0);
                break;
            case 1:
                PlayerPrefs.SetInt(selectedCharacter, 1); 
                knightRenderer.enabled = false;
                Knight.transform.position = offScreen;
                HeavyBandit.transform.position = characterPos;
                heavyBanditRenderer.enabled = true;
                characterInt++;
                break;
            case 2:
                PlayerPrefs.SetInt(selectedCharacter, 2); 
                heavyBanditRenderer.enabled = false;
                HeavyBandit.transform.position = offScreen;
                LightBandit.transform.position = characterPos;
                lightBanditRenderer.enabled = true;
                characterInt++;
                break;
            case 3:
                PlayerPrefs.SetInt(selectedCharacter, 3); 
                lightBanditRenderer.enabled = false;
                LightBandit.transform.position = offScreen;
                Knight.transform.position = characterPos;
                knightRenderer.enabled = true;
                characterInt++; 
                ResetInteger();
                break;
            default:
                PlayerPrefs.SetInt(selectedCharacter, 0);
                ResetInteger();
                break;
        }

    }

    public void PreviousCharacter(){
        switch(characterInt){
            case 0:
                PlayerPrefs.SetInt(selectedCharacter, 0);
                break;
            case 1:
                PlayerPrefs.SetInt(selectedCharacter, 2);
                knightRenderer.enabled = false;
                Knight.transform.position = offScreen;
                LightBandit.transform.position = characterPos;
                lightBanditRenderer.enabled = true;
                characterInt--; 
                ResetInteger();
                break;
            case 2:
                PlayerPrefs.SetInt(selectedCharacter, 3);
                heavyBanditRenderer.enabled = false;
                HeavyBandit.transform.position = offScreen;
                Knight.transform.position = characterPos;
                knightRenderer.enabled = true;
                characterInt--; 
                break;
            case 3:
                PlayerPrefs.SetInt(selectedCharacter, 1);
                lightBanditRenderer.enabled = false;
                LightBandit.transform.position = offScreen;
                HeavyBandit.transform.position = characterPos;
                heavyBanditRenderer.enabled = true;
                characterInt--;  
                break;
            default:
                PlayerPrefs.SetInt(selectedCharacter, 0);
                ResetInteger();
                break;
        }

    }

    void ResetInteger(){
        if(characterInt >= 4){
            characterInt = 1;
        }
        else if (characterInt <= 0) {
            characterInt = 3;
        }
    }

    public void changeScene() {
        SceneManager.LoadScene("Game");
    }
}
