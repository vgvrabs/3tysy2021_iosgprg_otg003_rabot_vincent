﻿using UnityEngine;

public class MoveBackground : MonoBehaviour {

    [SerializeField] Transform centerBackground;

    void Start() {
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        if(transform.position.x >= centerBackground.position.x + 10.94f){
            centerBackground.position = new Vector2(transform.position.x+ 10.94f, centerBackground.position.y);
        }
        else if (transform.position.x <= centerBackground.position.x - 10.94f){
            centerBackground.position = new Vector2 (transform.position.x - 10.94f, centerBackground.position.y);
        }
        
    }
}
