﻿using UnityEngine;

public class OppositeArrow : Arrow {
    void Start() {
        PickDirection();
        //parent = this.parent.transform.gameObject;
    }

    void Update() {
        //Compare();
    }

   public override void PickDirection(){
       randomDir = Random.Range(0,3) + 1;

        if(randomDir == 1){
            enemyDirection =  swipeDirection.Down;
            this.transform.Rotate(0,0,180);
        }
        else if(randomDir == 2){
            enemyDirection = swipeDirection.Up;
        }
        else if(randomDir == 3){
            enemyDirection = swipeDirection.Left; 
            this.transform.Rotate(0,0,90);       
        }
        else if(randomDir == 4){
            enemyDirection = swipeDirection.Right;
            this.transform.Rotate(0,0,270); 
        }

    }
}
