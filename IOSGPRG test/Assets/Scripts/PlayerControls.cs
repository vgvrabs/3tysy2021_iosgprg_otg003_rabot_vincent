﻿using UnityEngine;
using System.Collections;

public enum swipeDirection {
    Origin,
    Up,
    Down,
    Left,
    Right,
}

public class PlayerControls : MonoBehaviour {
    
    [SerializeField] Arrow arrow;
    [SerializeField] Enemy target;

    public float maxSwipetime;
    public float minSwipeDistance;
    public swipeDirection direction;
    public float dashSpeed;
    public float startDashTime;
    public float moveSpeed;
    public PlayerMana player;

    Rigidbody2D rb; 
    private float dashTime;
    private float swipeStartTime;
    private float swipeEndTime;
    private float swipeTime;
    private float swipeLength;
    private Vector2 startSwipePosition;
    private Vector2 endSwipePosition;
    bool isInRange = false;
    Vector2 move;

    void Move(){
        rb.transform.Translate(new Vector3(moveSpeed * Time.deltaTime, 0 ,0));
    }
    

    void OnTriggerEnter2D(Collider2D col){
        if(col.tag == "Enemy"){
            isInRange = true;
            target = col.GetComponent<Enemy>();
            arrow = col.GetComponentInChildren<Arrow>();
        }
    }

    void OnTriggerExit2D(){
        isInRange = false;
    }


    void Start() {
        //direction = null;
        dashTime = startDashTime;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update() {
        direction = swipeDirection.Origin;
        SwipeTest();
        Attack();
        Move();
    }


    void Attack(){
        if(isInRange){
            if(direction == arrow.enemyDirection && direction != swipeDirection.Origin) {
                target.death();
                player.addMana(target.getManaDrop());
            }
            if (direction != swipeDirection.Origin && direction != arrow.enemyDirection  && arrow.enemyDirection != swipeDirection.Origin) {
                target.Attack();
            }
        }
    }


    void SwipeTest(){
        if(Input.touchCount> 0){
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began){
                swipeStartTime = Time.time;
                startSwipePosition = touch.position;
            } 
            else if (touch.phase == TouchPhase.Ended){
                swipeEndTime = Time.time;
                endSwipePosition = touch.position;
                swipeTime = swipeEndTime - swipeStartTime;
                swipeLength = (endSwipePosition - startSwipePosition).magnitude;

                if(swipeTime<maxSwipetime && swipeLength > minSwipeDistance){
                    swipeControl();
                }
                else{
                    Debug.Log("tapped");
                    Dash();
                }

            }
        } 

    void swipeControl(){
        Vector2 distance = endSwipePosition - startSwipePosition;
        float xDistance = Mathf.Abs(distance.x);
        float yDistance = Mathf.Abs(distance.y);

        if(xDistance > yDistance){
            if(distance.x > 0){
                Debug.Log("Swiped Right"); direction = swipeDirection.Right;
            }
            else if(distance.x < 0){
                Debug.Log("Swiped Left"); direction = swipeDirection.Left;
            }
        }
        else if (yDistance > xDistance){
            if(distance.y > 0){
                Debug.Log("Swiped Up"); direction = swipeDirection.Up;
            }
            else if (distance.y < 0){
                Debug.Log("Swiped Down"); direction = swipeDirection.Down;
            }
        }
    }
    }

    void Dash() {
        rb.transform.Translate(new Vector3(dashSpeed * Time.deltaTime, 0 ,0));
    }
}

