﻿using UnityEngine;

public class Enemy: MonoBehaviour {
    Transform target;
    PlayerHealth targetHp;

    public int manaDrop = 10;
    public bool isInRange = false;
    public float atkCooldown;
    float timeForNextAtk;

    void OnTriggerEnter2D(Collider2D col){
        if(col.tag == "Player"){
            target = col.GetComponent<Transform>();
            targetHp = target.GetComponent<PlayerHealth>();
            isInRange = true;
        }
    }

    public void death(){
        this.gameObject.SetActive(false);
        Destroy(this.gameObject, 0.5f);
    }
    
    public int getManaDrop(){
        return manaDrop;
    }

    void checkDistance() {
        // seperate check distance
        if(target != null && Vector2.Distance(target.position, this.transform.position) <= 1 && timeForNextAtk <= 0) {
            Attack();
            timeForNextAtk = atkCooldown;
        }
        else if (timeForNextAtk > 0){
            timeForNextAtk -= Time.deltaTime;
        }
    }
    public void Attack(){
        timeForNextAtk = atkCooldown;
        targetHp.takeDamage(1);
    } 

    void Start(){
        atkCooldown = 1.5f;
        timeForNextAtk = atkCooldown;
    }

    void Update(){
        checkDistance();
        
        if (!isInRange){
            target = null;
            targetHp = null;
        }
    }

}
