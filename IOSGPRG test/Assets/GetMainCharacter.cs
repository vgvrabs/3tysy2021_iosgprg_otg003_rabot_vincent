﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMainCharacter : MonoBehaviour {

    public Sprite[] characters;
    SpriteRenderer mySprite;
    readonly string selectedCharacter = "SelectedCharacter";
    [SerializeField] int getMainCharacter;

    void Awake() {
        mySprite = this.GetComponent<SpriteRenderer>();
    }
    
    void Start() {

        int getCharacter = PlayerPrefs.GetInt(selectedCharacter);
        getMainCharacter = getCharacter;

        switch(getCharacter) {
            case 0:
                mySprite.sprite = characters[0];
                initializeKnight(mySprite);
                break;
            case 1:
                mySprite.sprite = characters[1];
                initializeHeavyBandit(mySprite);
                break;
            case 2:
                mySprite.sprite = characters[2];
                initializeLightBandit(mySprite);
                break;
            case 3:
                mySprite.sprite = characters[0];
                initializeKnight(mySprite);
                break;
            default:
                mySprite.sprite = characters[0];
                initializeKnight(mySprite);
                break;
        }
    }

    void initializeKnight(SpriteRenderer mySprite){
        mySprite.GetComponent<PlayerControls>().dashSpeed = 100;
        mySprite.GetComponent<PlayerControls>().moveSpeed = 2.5f;
        mySprite.GetComponent<Transform>().Rotate(0, 0, 0);
    }

    void initializeHeavyBandit(SpriteRenderer mySprite){
        mySprite.GetComponent<PlayerControls>().dashSpeed = 75;
        mySprite.GetComponent<PlayerControls>().moveSpeed = 2f;
        mySprite.flipX = true;
    }

    void initializeLightBandit(SpriteRenderer mySprite) {
        mySprite.GetComponent<PlayerControls>().dashSpeed = 125f;
        mySprite.GetComponent<PlayerControls>().moveSpeed = 3f; 
        mySprite.flipX = true;
    }

}
