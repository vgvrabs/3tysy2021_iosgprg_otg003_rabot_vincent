﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    public float rotSpeed;
    public VariableJoystick variableJoystick;
    public Rigidbody rb;


    public void FixedUpdate() {
        float heading = Mathf.Atan2(variableJoystick.Horizontal, variableJoystick.Vertical);
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        transform.LookAt(transform.position + direction);
        this.transform.Translate(direction *  Time.deltaTime * speed, Space.World);
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);
    }
}