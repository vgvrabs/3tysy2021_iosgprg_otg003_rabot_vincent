﻿
using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public GameObject[] enemies;
    public GameObject player;
    public Transform[] spawnPoints;
    public int enemyCount;

    public Vector3 spawnValues;
    public List<Transform> possibleSpawn = new List<Transform>();

    void PlayerSpawn()
    {
        Instantiate(player, new Vector3(0, 2, -5), Quaternion.identity);
    }
    void Spawn()
    {
        enemyCount = 10;
        
        
        while (enemyCount > 0)
        {
            int randomSpawn = Random.Range(0, enemies.Length);
            int spawnIndex = Random.Range(0, possibleSpawn.Count);

            Vector3 spawnPoint = new Vector3();

            GameObject spawnedEnemy =  Instantiate(enemies[randomSpawn], possibleSpawn[spawnIndex].position, Quaternion.identity);
            possibleSpawn.Remove(possibleSpawn[spawnIndex]);
            enemyCount--;
        }
    }

    void TestSpawn()
    {
        float spawnX = Random.Range(-50, 50);
        float spawnZ = Random.Range(-50, 50);
        Vector3 spawnPoint = new Vector3(0, 2, 5);
        GameObject spawnedEnemy = Instantiate(enemies[0], spawnPoint, Quaternion.identity);
    }
    void Start()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            possibleSpawn.Add(spawnPoints[i]);
        }
        Spawn();
        //TestSpawn();
    }
}
