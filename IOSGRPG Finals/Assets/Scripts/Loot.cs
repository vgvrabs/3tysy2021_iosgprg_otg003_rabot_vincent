﻿using UnityEngine;

public class Loot : MonoBehaviour {

    public int bulletLoot;

   public virtual void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            other.GetComponent<Unit>().pistolAmmo += bulletLoot;
        }
        Destroy(this.gameObject);
    }
}   
