﻿
using UnityEngine;

public class PistolAmmoLoot : AmmoLoot
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            other.GetComponent<Player>().pistolAmmo += bulletCount;
            if(other.GetComponent<Player>().currentWeapon != null && other.gameObject.name == "Player")
            {
                other.GetComponent<Player>().Reload();
                other.GetComponent<Player>().DisplayAmmo();
            }
            Destroy(this.gameObject);
        }
    }

}
