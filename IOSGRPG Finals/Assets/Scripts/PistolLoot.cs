﻿
using UnityEngine;

public class PistolLoot : WeaponLoot
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            other.GetComponent<Player>().PickUpWeapon(weaponLoot);
        }
        Destroy(this.gameObject);
    }
}
