﻿using System;
using System.Net;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AIControl : Player
{
    private NavMeshAgent _agent;
    public float lookRadius = 10f;
    public float aggroRange = 20f;
    public float lootRange = 100f;
    public GameObject target;
    public JoystickPlayerExample playerMovement;
    public bool isAttacking;
    public GameObject[] enemies;
    public GameObject[] lootables;
    public GameObject loot;
    public bool hasWeapon;
    public bool isWandering;
    public bool isFleeing;
    public Transform mySpawnPoint;
    void Start()
    {
        _agent = this.GetComponent<NavMeshAgent>();

        if (!CheckWeapon())
        {
            FindLoot();
        }
        
        isWandering = false;
        
        FindEnemy();
        
        //playerMovement = target.GetComponent<JoystickPlayerExample>();
        currentWeapon = GetComponentInChildren<Weapon>();
     
    }

    public bool CheckWeapon()
    {
        if (currentWeapon != null)
        {
            hasWeapon = true;
        }
        return hasWeapon;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    

    void Seek(Vector3 location)
    {
        _agent.SetDestination(location);
    }

    void Flee(Vector3 location)
    {
        Vector3 fleeDirection = location - this.transform.position;
        _agent.SetDestination(this.transform.position - fleeDirection);
        isFleeing = true;
    }
    
    void Pursue()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;

        float lookAhead = targetDirection.magnitude / (_agent.speed);
        
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }
    void Attack(Transform currentTarget)
    {
        _agent.isStopped = true;
        
        isAttacking = true;
        this.transform.LookAt(currentTarget.transform.position);
        this.currentWeapon.Shoot();
    }

    private Vector3 _wanderTarget;

    void Wander()
    {
        float wanderRadius = 20f;
        float wanderDistance = 5f;
        float wanderJitter = 3f;
        float interval = Random.Range(2, 10);
        //Invoke("Wander", interval);
        _wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter,
            0,
            Random.Range(-1.0f, 1.0f) * wanderJitter);
        _wanderTarget.Normalize();
        _wanderTarget *= wanderRadius;

        Vector3 targetLocal = _wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

        Seek(targetWorld);
    }

    public override void Reload()
    {
        StartCoroutine(currentWeapon.Reload());
    }
    
    void Behavior()
    {

        if (!CheckWeapon())
        {
            FindLoot();
        }

        if (target == null)
        {
            isWandering = true;
            _agent.isStopped = false;
            isAttacking = false;
            FindEnemy();
        }

        if (target != null && hasWeapon)
        {
            if (Vector3.Distance(this.transform.position, target.transform.position) <= aggroRange)
            {
                Pursue();
                if (Vector3.Distance(this.transform.position, target.transform.position) <= aggroRange &&
                    hasWeapon)
                {
                    CancelInvoke();
                    if (currentWeapon.hasBullets)
                    {
                        Attack(target.transform);
                    }
                    else if (!currentWeapon.hasBullets)
                    {
                        Reload();
                    }
                }
            }
            else
            {
                //isWandering = true;
                _agent.isStopped = false;
                isAttacking = false; //Wander();
            }
        }
        else if (target != null && !hasWeapon)
        {
            Flee(target.transform.position);
        }

        if (hasWeapon && target == null)
        {
            Wander();
        }
    }

    void FindEnemy()
    {
        enemies = GameObject.FindGameObjectsWithTag("Unit");
            int enemyCount = enemies.Length;
            
            foreach (GameObject enemy in enemies)
            {
                if (enemy.Equals(this.gameObject))
                    continue;

                if (Vector3.Distance(this.transform.position, enemy.transform.position) <= aggroRange)
                {
                    target = enemy;
                }
                else
                {
                    //isWandering = true;
                }
            }
    }

    public override void PickUpWeapon(GameObject weapon)
    {
        if (currentWeapon == null)
        {
            GameObject chosenWeapon = Instantiate(weapon, weaponPos.position, transform.rotation);
            currentWeapon = chosenWeapon.GetComponent<Weapon>();
            currentWeapon.transform.parent = weaponPos;
            hasWeapon = true;
            isWandering = true;
            loot = null;
        }
    }

    void FindLoot()
    {
        lootables = GameObject.FindGameObjectsWithTag("WeaponLoot");
        int lootCount = lootables.Length;

        foreach (GameObject item in lootables)
        {
            if (Vector3.Distance(this.transform.position, item.transform.position) <= lootRange)
            {
                loot = item;
            }
        }
        Seek(loot.transform.position);
    }
    
    void Update()
    {
        Behavior(); //FindEnemy();
    }
}
