﻿
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int damage;
    public float speed;

    void Update(){
        //bullet will travel with respect to where player is lookin
        this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
        Destroy(this.gameObject, 2f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Projectile"))
        {
            if (other.CompareTag("Unit"))
            {
                other.GetComponent<Health>().TakeDamage(damage);
            }
            Destroy(this.gameObject);
        }

    }
    
}
