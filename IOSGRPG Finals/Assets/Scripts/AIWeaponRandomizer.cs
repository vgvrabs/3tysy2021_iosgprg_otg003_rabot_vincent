﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class AIWeaponRandomizer : WeaponSwitching
{
    // Start is called before the first frame update
    public AIControl parent;
    void Start()
    {
        parent = GetComponentInParent<AIControl>();
        selectedWeapon = Random.Range(0, 2);
        SelectWeapon();
    }

    protected override void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
                parent.currentWeapon = weapon.GetComponent<Weapon>();
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }
}
