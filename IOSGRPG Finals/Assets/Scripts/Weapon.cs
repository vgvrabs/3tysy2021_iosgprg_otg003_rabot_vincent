using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour {
    public GameObject projectilePrefab;
    public Transform firePoint;
    [SerializeField] protected Player playerRef;

    public bool hasBullets;
    public int currentAmmo;

    public int bulletPerShot;

    public int reserveAmmo = 0; 
    public int maxAmmo;
    public float fireRate;
    public float spreadAngle;
    public int reloadTime = 2;
    [SerializeField] protected float nextFire = 0f;

    protected GameObject p;


    void Start()
    {
        playerRef = transform.root.GetComponent<Player>();
        playerRef.DisplayAmmo();
        reserveAmmo = playerRef.pistolAmmo;
        playerRef.pistolAmmo = 0;
    }
    
    

    protected void CheckAmmo(){
        if (currentAmmo < bulletPerShot)
        {
            hasBullets = false;
        }

        if(currentAmmo >= bulletPerShot){
            hasBullets = true;
        }
    }
    public virtual void Shoot(){
        if(hasBullets && Time.time > nextFire) {
            nextFire = Time.time + fireRate;
            Instantiate(projectilePrefab, firePoint.position, this.transform.rotation);
            currentAmmo -= bulletPerShot;
            CheckAmmo();
        }
    }
    

    public virtual IEnumerator Reload(){   
        yield return new  WaitForSeconds(reloadTime);

        if (currentAmmo < bulletPerShot && reserveAmmo >= maxAmmo)
        {
            currentAmmo = maxAmmo;
            reserveAmmo -= maxAmmo;
        }
        
        if (reserveAmmo == 0)
        {
            reserveAmmo = playerRef.pistolAmmo;
            playerRef.pistolAmmo = 0;
        }
        reserveAmmo = Mathf.Clamp(reserveAmmo, 0, 60);
        CheckAmmo();
    }
}
