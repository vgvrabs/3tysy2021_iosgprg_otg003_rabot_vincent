﻿
using UnityEngine;

public class WeaponSwitching : MonoBehaviour
{
    private Player _player;
    public int selectedWeapon = 0;

    void Start()
    {
        _player = GetComponentInParent<Player>();
        SelectWeapon();
    }

    private void Update()
    {
        SelectWeapon();
    }

    protected virtual void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
                _player.currentWeapon = weapon.GetComponent<Weapon>();
                _player.DisplayAmmo();
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }

    public void ChangeWeapon()
    {
        selectedWeapon++;

        if (selectedWeapon > 2)
        {
            selectedWeapon = 0;
        }
    }
}
