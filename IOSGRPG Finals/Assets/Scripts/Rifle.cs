﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Rifle : Weapon
{


    void Start()
    {
        playerRef = transform.root.GetComponent<Player>();
        playerRef.DisplayAmmo();
        reserveAmmo = playerRef.rifleAmmo;
        playerRef.rifleAmmo = 0;
    }
    
    public override IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
         
        if (currentAmmo < bulletPerShot && reserveAmmo >= maxAmmo)
        {
            currentAmmo = maxAmmo;
            reserveAmmo -= maxAmmo;
        }

        if (reserveAmmo == 0)
        {
            reserveAmmo = playerRef.rifleAmmo;
            playerRef.rifleAmmo = 0;
        }
        reserveAmmo = Mathf.Clamp(reserveAmmo, 0, 120);
        CheckAmmo();
    }
    public override void Shoot()
    {
        if(hasBullets && Time.time > nextFire){
            nextFire = Time.time + fireRate;
            p = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
            p.transform.rotation = Quaternion.RotateTowards(p.transform.rotation, this.transform.rotation, spreadAngle);
            currentAmmo -= bulletPerShot;
            CheckAmmo();
        }
    }
}
