﻿using UnityEngine;

public class PrimaryWeaponLoot : SecondaryWeaponLoot{
    public override void OnTriggerEnter(Collider other){
        if(other.tag == "Player"){
            other.GetComponent<Unit>().PickUpWeapon(loot);
            other.GetComponent<Unit>().primaryWeapon = loot;
        }
        Destroy(gameObject);
    }
}
