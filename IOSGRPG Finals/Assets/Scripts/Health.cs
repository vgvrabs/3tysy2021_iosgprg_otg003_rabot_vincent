﻿using UnityEngine;

public class Health : MonoBehaviour {

    public int currentHp;
    public int maxHp;
    public bool isAlive = true;
    

    void Start() {
        currentHp = maxHp;
    }

    public virtual void TakeDamage(int value) {
        currentHp -= value;
        CheckIfAlive();
        // update HP ui
    }
    
    void AddHp(int value)
    {
        currentHp += value;
        // update HP ui
    }

    public virtual bool CheckIfAlive()
    {
        if (currentHp <= 0)
        {
            isAlive = false;
            Destroy(this.gameObject);
        }
        return isAlive;
    }
}
