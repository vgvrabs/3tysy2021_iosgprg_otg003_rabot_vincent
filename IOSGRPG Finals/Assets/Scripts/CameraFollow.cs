﻿
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.3f;

    Vector3 velocity = Vector3.zero;
   
    // Update is called once per frame

    void Start()
    {
        //target = GameObject.Find("Player(Clone)").GetComponent<Transform>();
    }
    void FixedUpdate()
    {
        Vector3 goalPos = target.position;
        goalPos.y = transform.position.y;
        transform.position = Vector3.SmoothDamp(transform.position, goalPos, ref velocity, smoothTime);
        
    }
}
