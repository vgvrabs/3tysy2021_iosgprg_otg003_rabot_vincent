﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Unit : MonoBehaviour
{
    public GameObject secondaryWeapon;
    public GameObject primaryWeapon;
    public GameObject currentWeapon;

    public Transform weaponPos;
    public TextMeshProUGUI ammoText;
    public int maxHp;

    public int pistolAmmo;
    public int rifleAmmo;

    public int shotgunShells;
    
    int currentHp;

    void Start() {
        currentHp = maxHp;
        DisplayAmmo();
    }

    public void DisplayAmmo(){
        if(currentWeapon != null){
            ammoText.text = "Ammo:" + currentWeapon.GetComponent<Weapon>().currentAmmo + "/" + currentWeapon.GetComponent<Weapon>().reserveAmmo;
        }
    }

    public void TakeDamage(int damage) {
        currentHp -= damage;
        Mathf.Clamp(currentHp, 0, maxHp);
        if(currentHp <= 0){
            Destroy(gameObject);
        }
    }
    public void Shoot(){
        if(!currentWeapon.GetComponent<Weapon>().hasBullets){
            Reload();
        }
        currentWeapon.GetComponent<Weapon>().Shoot();
        DisplayAmmo();
    }

    public void Reload(){
        currentWeapon.GetComponent<Weapon>().Reload();
        DisplayAmmo();
    }

    public void PickUpWeapon(GameObject weapon){
        if(currentWeapon != null){
            currentWeapon.gameObject.SetActive(false);
        }
        currentWeapon = Instantiate(weapon, weaponPos.position, transform.rotation);
        currentWeapon.transform.parent = weaponPos;
        DisplayAmmo();
    }
}
