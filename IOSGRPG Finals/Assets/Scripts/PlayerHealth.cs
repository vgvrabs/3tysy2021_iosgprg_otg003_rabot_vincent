﻿
using UnityEngine;

public class PlayerHealth : Health
{
    public HealthBar healthBar;

    void Start()
    {
        isAlive = true;
        healthBar = GameObject.FindGameObjectWithTag("HealthCanvas").GetComponent<HealthBar>();
        currentHp = maxHp;
        healthBar.setMaxHealth(maxHp);
    }
    public override void TakeDamage(int value)
    {
        currentHp -= value;
        healthBar.setHealth(currentHp);
        CheckIfAlive();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            this.TakeDamage(50);
        }
    }
    public override bool CheckIfAlive()
    {
        if (currentHp <= 0)
        {
            isAlive = false;
            this.gameObject.SetActive(false);
        }
        return isAlive;
    }
}
