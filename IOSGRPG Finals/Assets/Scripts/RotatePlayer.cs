﻿
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{

    public float rotSpeed;
    public VariableJoystick variableJoystick;

    void Start()
    {
        variableJoystick = GameObject.FindGameObjectWithTag("RotateJoystick").GetComponent<VariableJoystick>();
    }

    void FixedUpdate() 
    {
        float heading = Mathf.Atan2(variableJoystick.Horizontal, variableJoystick.Vertical);
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        transform.LookAt(transform.position + direction);
    }
}
