﻿
using UnityEngine;

public class ShotgunShellsLoot : AmmoLoot
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            other.GetComponent<Player>().shotgunShells += bulletCount;
            
            if (other.gameObject.name == "Player" && other.GetComponent<Player>().currentWeapon != null)
            {
                other.GetComponent<Player>().Reload();
                other.GetComponent<Player>().DisplayAmmo();
            }
            Destroy(this.gameObject);
        }
    }
}
