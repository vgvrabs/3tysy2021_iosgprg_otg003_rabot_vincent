﻿using UnityEngine;

public class SecondaryWeaponLoot : Loot
{
    public GameObject loot;

    public override void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){
            if(other.GetComponent<Unit>().secondaryWeapon == null){
                other.GetComponent<Unit>().PickUpWeapon(loot);
                other.GetComponent<Unit>().secondaryWeapon = loot;
        }
        Destroy(gameObject);
    }

    }   
}