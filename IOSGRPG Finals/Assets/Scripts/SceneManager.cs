﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public PlayerHealth player;
    public GameObject deathUI;

    public void PlayerDeath()
    {
        if (!player.CheckIfAlive())
        {
            deathUI.SetActive(true);
        }
    }

    public void LoadMainScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void LoadMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    void Start()
    {
        deathUI.SetActive(false);
        PlayerDeath();
    }

    void Update()
    {
        if(player != null)
            PlayerDeath();
    }

}
