﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    public int bulletCount;
    private List<Quaternion> _bullets;

    void Start()
    {
        playerRef = transform.root.GetComponent<Player>();
        playerRef.DisplayAmmo();
        reserveAmmo = playerRef.shotgunShells;
        playerRef.shotgunShells = 0;
        _bullets = new List<Quaternion>(bulletCount);
        for(int i = 0; i < bulletCount; i++){
            _bullets.Add(Quaternion.Euler(Vector3.zero));
        }
        
    }
    
    public override IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
        
        if (currentAmmo < bulletPerShot && reserveAmmo >= maxAmmo)
        {
            currentAmmo = maxAmmo;
            reserveAmmo -= maxAmmo;
        }

        if (reserveAmmo == 0)
        {
            reserveAmmo = playerRef.shotgunShells;
            playerRef.shotgunShells = 0;
        }

        reserveAmmo = Mathf.Clamp(reserveAmmo, 0, 60);
        CheckAmmo();
    }

    public override void Shoot(){
        if(hasBullets && Time.time > nextFire) {
            nextFire = Time.time + fireRate;
            for(int i = 0; i < bulletCount; i++){
                _bullets[i] = Random.rotation;
                p = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
                p.transform.rotation = Quaternion.RotateTowards(p.transform.rotation, _bullets[i], spreadAngle);
            }
            currentAmmo -= bulletPerShot;
            CheckAmmo();
        }
      
    }
}
