﻿
using UnityEngine;

public class RifleLoot : WeaponLoot
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            other.GetComponent<Player>().PickUpWeapon(weaponLoot);
        }
        Destroy(this.gameObject);
    }
}
