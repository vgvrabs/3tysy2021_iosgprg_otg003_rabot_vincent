﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    public Weapon currentWeapon;

    public Transform weaponPos;
    public TextMeshProUGUI ammoText;

    public int pistolAmmo;
    public int rifleAmmo;
    public int shotgunShells;
    
    int currentHp;
    [SerializeField] bool isShooting = false;

    void Start()
    {
        ammoText = GameObject.FindGameObjectWithTag("AmmoCanvas").GetComponent<TextMeshProUGUI>();
        //this.transform.position = new Vector3(0, 2, -5);
        //DisplayAmmo();
    }

    void Update() {
        DisplayAmmo();
        if (isShooting){
            Shoot();
        }
    }

    public virtual void DisplayAmmo(){
        if (currentWeapon != null)
        {
            ammoText.text = "Ammo:" + currentWeapon.currentAmmo + "/" + currentWeapon.reserveAmmo;
        }
    }

    public void Shoot(){
        isShooting = true;
        if(!currentWeapon.hasBullets){
            Reload();
        }
        //currentWeapon.GetComponent<Shotgun>().Shoot();
        currentWeapon.Shoot();
        DisplayAmmo();
    }

    public void stopShoot(){
        isShooting = false; 
    }

    public virtual void Reload()
    {
        StartCoroutine(currentWeapon.Reload());
        DisplayAmmo();
    }


    public void PickUpAmmo()
    {
        DisplayAmmo();
    }
    

    public virtual void PickUpWeapon(GameObject weapon)
    {
        GameObject chosenWeapon = Instantiate(weapon, weaponPos.position, transform.rotation);
        currentWeapon = chosenWeapon.GetComponent<Weapon>();
        currentWeapon.transform.parent = weaponPos;
    }
}
